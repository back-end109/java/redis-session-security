package dev.pro.redissession.controller;

// import org.springframework.boot.actuate.web.exchanges.HttpExchange.Principal;
import java.security.Principal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public String home(Principal principal) {
        System.out.println(principal.getName());
        return "Hello, " +  principal.getName();
    }
}
